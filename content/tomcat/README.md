# Tomcat

## Run Server


- make sure MySQL is running
- go to directory

```
cd /usr/local/tomcat/apache-tomcat-9.0.11
```

- start the application

```
bin/catalina.sh start
```

- go to page

```
http://localhost:8080
```
