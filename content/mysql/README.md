# MySQL

## Run Server

- start the server

```
sudo rcmysql start
```

- check the status

```
sudo rcmysql status
```

- stop the server

```
sudo rcmysql stop
```
