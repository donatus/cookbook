# PDF

## Extract Pages

#### pdfjam

- [src](https://tex.stackexchange.com/questions/79623/quickly-extracting-individual-pages-from-a-document)

You can use the pdfjam tool with the syntax

`pdfjam <input file> <page ranges> -o <output file>`

and an example of page ranges would be

`3,67-70,80`

to extract page 3, pages 67 to 70 and page 80, and put these pages into a single document. Note however that this will break the hyperlinks in your document.

pdfjam is actually a front end for pdfpages, suggested in an other answer.

#### pdfseparate

You could also use `pdfseparate` from poppler to burst a document into separate pages. Using

`pdfseparate <document> 'page_%d.pdf'`

using the result of pdfjam as a document will give you files named `page_1.pdf ... page_6.pdf` containing all the desired pages.

## Merge Documents

#### pdfunite

- [src](https://stackoverflow.com/questions/2507766/merge-convert-multiple-pdf-files-into-one-pdf)

Considering that pdfunite is part of poppler it has a higher chance to be installed, usage is also simpler than pdftk:

`pdfunite in-1.pdf in-2.pdf in-n.pdf out.pdf`


You can use: `pdfunite *.pdf out.pdf` assuming no other pdf exists in that directory and their order is preserved by "\*".
