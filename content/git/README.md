# Git

## Repository

```
git init
git config --local user.name "John Doe"
git config --local user.email johndoe@example.com
git config --local core.editor emacs
git config --list

git config user.name
John Doe
```

Quick setup — if you’ve done this kind of thing before
or

HTTPS `https://github.com/johndoe/project.git`
SSH `git@github.com:johndoe/project.git`

We recommend every repository include a README, LICENSE, and .gitignore.

... or create a new repository on the command line

```
echo "# project" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/johndoe/project.git
git push -u origin master
```

... or push an existing repository from the command line

```
git remote add origin https://github.com/hekrott/biby.git
git push -u origin master
```


... or import code from another repository
You can initialize this repository with code from a Subversion, Mercurial, or TFS project.

```
git pull origin master
```


## [How do I force “git pull” to overwrite local files?](https://stackoverflow.com/questions/1125968/how-do-i-force-git-pull-to-overwrite-local-files)


I think this is the right way:

```
git fetch --all
```

Then, you have two options:

```
git reset --hard origin/master
```

OR If you are on some other branch:

```
git reset --hard origin/<branch_name>
```
